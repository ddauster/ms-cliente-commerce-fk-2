#!/bin/bash

#verificar se app subiu
until curl -s -I http://localhost:8080 | grep 200 > /dev/null
do
      echo "Aplicação ainda não está no ar"
      sleep 10
done
echo "Aplicação está online"


#start a pipeline de teste
StCodeStart=$(curl -o /dev/null -s -w "%{http_code}"  --request POST \
   --url http://localhost:8080/unittests/start \
   --header 'Content-Type: application/json' \
   --data '{"password":"123456"}')

if [ $StCodeStart = 204 ]; then
    echo "Start nos testes"
else 
    exit 1
fi

#esperar rodar pipeline de teste
echo "Esperando rodar os testes"
#sleep 86

#verificar o status da pipeline de teste
StCodeStatus=$(curl -o status.json -s -w "%{http_code}"  --request POST \
   --url http://localhost:8080/unittests/status \
   --header 'Content-Type: application/json' \
   --data '{"password":"123456"}')

if [ $StCodeStatus = 200 ]; then
    echo "Verificando o status dos testes"
else 
    rm -rf status.json 
    exit 1
fi
 
sed -i 's/,//g' status.json 
sed -i 's/ //g' status.json

Erros=$(cat status.json | grep failures | awk -F ':' '{print $2}')

rm -rf status.json

echo "Foram encontrados ${Erros} erro(s) na fase de testes."

if [ $Erros = 0 ]; then
    echo "Chuvaaaa  !!"
else 
    exit 1
fi